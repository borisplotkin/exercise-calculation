function GetXmlHttpObject(){
	var xmlHttp=null;
	try	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp=new XMLHttpRequest();
	}	catch (e)	{
		// Internet Explorer
		try		{
			xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
		}		catch (e)		{
			xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}
function changearea() { 
	if (xmlHttp.readyState==4)	{ 
		document.getElementById("changearea").innerHTML=xmlHttp.responseText;
	}
}
function takeaction(){
	xmlHttp=GetXmlHttpObject();
	if (xmlHttp==null)	{
		alert ("Your browser does not support AJAX!");
		return;
	}
   	postValue = new Array();
	postValue[0] = document.getElementById("D1").value;
	postValue[1] = document.getElementById("D2").value;
	postValue[2] = document.getElementById("D3").value;
	postValue[3] = document.getElementById("D4").value;
	postValue[4] = document.getElementById("D5").value;
	postValue[5] = document.getElementById("D6").value;
	postValue[6] = document.getElementById("D7").value;
	postValue[7] = document.getElementById("D8").value;
	postValue[8] = document.getElementById("D9").value;
	var url="calc.php?values="+postValue;
	xmlHttp.onreadystatechange=changearea;
	xmlHttp.open("GET",url,true);
	xmlHttp.send(null);
}
function takeactionnc(){
	var order = document.getElementById("D10").value;
	var sets = document.getElementById("D11").value;
	var reps = document.getElementById("D12").value;
	$("#sets").load("calc.php?sets="+sets);
	$("#reps").load("calc.php?reps="+reps);
}
function changefield(str){
	myStr = str.split(',');
	id = myStr[0];
	muscle = myStr[1];
	$("#change1"+id).load("calc.php?change1=1&id="+id+"&musclegroup="+muscle);
	$("#change2"+id).load("calc.php?change2=2&id="+id+"&musclegroup="+muscle);
}
function del(id){
	$("#admin").load("calc.php?admin=1&del=1&id="+id);
}
function add(){
	var exercise = escape(document.getElementById("exercise").value);
	var exercisetype = escape(document.getElementById("exercisetype").value);
	var musclegroup = escape(document.getElementById("musclegroup").value);
	$("#admin").load("calc.php?admin=1&add=1&exercise="+exercise+"&exercisetype="+exercisetype+"&musclegroup="+musclegroup);
}

function changefieldarea(id) { 
	if (xmlHttp.readyState==4)	{ 
		document.getElementById("change"+id).innerHTML=xmlHttp.responseText;
	}
}