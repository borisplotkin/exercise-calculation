<?php

define('HOST','localhost');
define('USERNAME','root');
define('PASSWORD','');
define('DB_NAME','ex');

/**
* Function Description: Database Connection
*
* @return variable $connection
*/
function db_connect() {   	
	//Connect to database server
	$connection = mysql_connect(HOST,USERNAME,PASSWORD);	
	//testing to see the connection works
	if (!$connection){
	   die('Error: '.mysql_error());	
	}	
	//Selecting the database name and testing if it exists
	if (!mysql_select_db(DB_NAME)){
	   die('Error: '. mysql_error());	
	}	
	return $connection;	
}

/**
* Function Description: Pass the value from databse into an array
* @Param: $result
* @return: $result_array
*/
function db_result($result)    {
	$result_array = array();   
		for ($i=0; @$row = mysql_fetch_array($result) ; $i++)       	{
		   $result_array[$i] = $row;
		}		
	return $result_array;
}
?>