<html>

<head>
<meta http-equiv="Content-Language" content="en-ca">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Breven's Workout Generator</title>
<script type="text/javascript" src="jquery.min.js"></script>
<script type="text/javascript" src="custom.js"></script>
<style>
<!--
a:link { color:#C0C0C0;}
a:visited {color:#C0C0C0;}
a:hover { color:#FFFFFF;}
-->
</STYLE>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0">

<div align="center">

<table cellpadding="0" cellspacing="0" width="100%" height="30">
	<!-- MSTableType="nolayout" -->
	<tr>
		<td align="left" bgcolor="#000000" valign="middle"><b>
		<font face="Arial" size="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="text-decoration: none">
		<font color="#FFFFFF">Home</font></span></font></b></td>
	</tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>

<table border="0" cellpadding="0" cellspacing="0" width="479" height="414">
	<!-- MSTableType="nolayout" -->
	<tr>
		<td valign="top" width="479" style="border-top-style: none; border-top-width: medium">
		<!-- MSCellType="ContentBody" -->
		<table cellpadding="0" cellspacing="0" width="861" height="375" style="border-width:0px; ">
			<!-- MSTableType="nolayout" -->
			<tr>
				<td width="86" valign="middle" style="border-right-style: none; border-right-width: medium; border-left-style:solid; border-left-width:1px; border-top-style:solid; border-top-width:1px; border-bottom-style:solid; border-bottom-width:1px" bgcolor="#5A6D73" align="center">
				<p align="center"><b>
		<font face="Arial" size="2" color="#FFFFFF">Muscle Group</font></b></td>
				<td height="41" width="81" style="border-left:1px solid #FFFFFF; border-right-style: solid; border-right-width: 1px; border-top-style:solid; border-top-width:1px; border-bottom-style:solid; border-bottom-width:1px; " bgcolor="#5A6D73" align="center" bordercolorlight="#FFFFFF">
				<p align="center"><b>
				<font face="Arial" size="2" color="#FFFFFF">Number of 
				Exercises</font></b></td>
				<td width="11" valign="top" style="border-left-style:solid; border-left-width:1px; border-top-style:none; border-top-width:medium; border-bottom-style:none; border-bottom-width:medium" rowspan="2">
				&nbsp;</td>
				<td height="375" width="676" valign="top" style="border-style:solid; border-width:1px; " bgcolor="#F2EFF5" rowspan="2" id="changearea">
					<!--webbot bot="SaveResults" S-Label-Fields="TRUE" -->
					<p align="center">
		&nbsp;</p>
					<p align="center">
		&nbsp;</p>
					<p align="center">
		<font color="#5A6D73" size="6" face="Calibri">To begin, please choose 
		the muscle groups<br>
		you wish to work on today.</font></p>
					<p align="center">
		<font color="#5A6D73" size="6" face="Calibri">
		<br>
		Have a great workout!<br>
		</font>&nbsp;</p>
				</td></span>
			</tr>
			<tr>
				<td width="86" valign="top" style="border-right-style: none; border-right-width: medium; border-left-style:solid; border-left-width:1px; border-top-style:solid; border-top-width:1px; border-bottom-style:solid; border-bottom-width:1px" bgcolor="#F2EFF5">
					<p style="margin-bottom: -3px" align="right">&nbsp;</p>
					<p style="margin-bottom: -3px" align="right">
					<font size="2" face="Arial"> 
					<b>&nbsp;Abdominals</b></font></p>
					<p style="margin-bottom: -3px" align="right"><b>
					<font size="2" face="Arial"> 
					Back</font></b></p>
					<p style="margin-bottom: -3px" align="right"><b>
					<font size="2" face="Arial"> 
					Biceps</font></b></p>
					<p style="margin-bottom: -3px" align="right"><b>
					<font size="2" face="Arial"> 
					Calves</font></b></p>
					<p style="margin-bottom: -3px" align="right"><b>
					<font size="2" face="Arial"> 
					Chest</font></b></p>
					<p style="margin-bottom: -3px" align="right"><b>
					<font size="2" face="Arial"> 
					Hamstrings</font></b></p>
					<p style="margin-bottom: -3px" align="right"><b>
					<font size="2" face="Arial"> 
					Shoulders</font></b></p>
					<p style="margin-bottom: -3px" align="right"><b><font face="Arial" size="2">Triceps</font></b></p>
					<p style="margin-bottom: -3px" align="right"><font face="Arial" size="2"><b>
					Quads</b></font></p>
				</td>
				<td height="327" width="81" valign="top" style="border-right-style: solid; border-right-width: 1px; border-top-style:solid; border-top-width:1px; border-bottom-style:solid; border-bottom-width:1px; border-left-style:none; border-left-width:medium" bgcolor="#F2EFF5">
				
					<!--webbot bot="SaveResults" U-File="C:\Users\MB Consulting\Desktop\_private\form_results.csv" S-Format="TEXT/CSV" S-Label-Fields="TRUE" -->
					<p style="margin-bottom: -9px" align="center">
					<font face="Arial">
					<br>
					<br>
					<select size="1" id="D1" onchange="takeaction()">
					<option value="">-</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">8</option>
					<option value="9">9</option>
					<option value="10">10</option>
					</select></font></p>
					<p style="margin-bottom: -9px" align="center"><font face="Arial">
					<select size="1" id="D2" onchange="takeaction()">
					<option value="">-</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">8</option>
					<option value="9">9</option>
					<option value="10">10</option>
					</select></font></p>
					<p style="margin-bottom: -9px" align="center"><font face="Arial">
					<select size="1" id="D3" onchange="takeaction()">
					<option value="">-</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">8</option>
					<option value="9">9</option>
					<option value="10">10</option>
					</select></font></p>
					<p style="margin-bottom: -9px" align="center"><font face="Arial">
					<select size="1" id="D4" onchange="takeaction()">
					<option value="">-</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">8</option>
					<option value="9">9</option>
					<option value="10">10</option>
					</select></font></p>
					<p style="margin-bottom: -9px" align="center"><font face="Arial">
					<select size="1" id="D5" onchange="takeaction()">
					<option value="">-</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">8</option>
					<option value="9">9</option>
					<option value="10">10</option>
					</select></font></p>
					<p style="margin-bottom: -9px" align="center"><font face="Arial">
					<select size="1" id="D6" onchange="takeaction()">
					<option value="">-</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">8</option>
					<option value="9">9</option>
					<option value="10">10</option>
					</select></font></p>
					<p style="margin-bottom: -9px" align="center"><font face="Arial">
					<select size="1" id="D7" onchange="takeaction()">
					<option value="">-</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">8</option>
					<option value="9">9</option>
					<option value="10">10</option>
					</select></font></p>
					<p style="margin-bottom: -10px" align="center"><font face="Arial">
					<select size="1" id="D8" onchange="takeaction()">
					<option value="">-</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">8</option>
					<option value="9">9</option>
					<option value="10">10</option>
					</select></font></p>
					<p style="margin-bottom: -10px" align="center">
					<font face="Arial">
					<select size="1" id="D9" onchange="takeaction()">
					<option value="">-</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">8</option>
					<option value="9">9</option>
					<option value="10">10</option>
					</select></font></p>
				</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
<?php  if($_GET['admin']){ include('calc.php');} ?>
</div>
</body>

</html>
